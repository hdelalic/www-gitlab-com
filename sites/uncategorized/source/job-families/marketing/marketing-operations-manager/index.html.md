---
layout: job_family_page
title: "Marketing Operations Manager"
---

You understand that setting a foundation for healthy growth in a fast-paced company is effective marketing operations. In all cases, you share our [values](https://about.gitlab.com/handbook/values/)

## Associate Marketing Operations Manager
Your job is to support the Marketing Operations team in meeting our goals of optimizing technology to enable effective marketing at GitLab, ensuring high data quality and helping colleagues access that data to enable smarter decisions, and assisting in marketing analysis, planning and strategy.  This is an entry level position for Marketing Operations.

### Job Grade 

The Associate Marketing Operations Manager is a [grade 5](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Responsibilities

Marketing technology
*  Learning tools that marketing has in place to act as backup on various systems as needed
*  Provisioning licenses for base entitlements to appropriate GitLab team members
*  Providing oversight on GitLab issues boards - building weekly standup issues, creating biweekly milestone issues to ensure handbook updates are completed, documenting minutes from meetings, and ensuring labels exist on all Marketing issues

Marketing data stewardship
*  Assisting with data cleanliness in the various systems, merging like records, researching causes of bugs in order to correct processes
*  Cleansing, enriching and uploading prospect lists from various events
*  Building smart lists in Marketo to aid in geographic and account based marketing efforts

Marketing analysis
*  Providing support to team with ad hoc analysis and/or the underlying data as needed

### Requirements

*  Exemplary spoken and written English.
*  Experience in sales and/or marketing teams of B2B software, Open Source software, and the developer tools space is preferred.
*  Experience with marketing automation software a plus
*  Experience with Salesforce CRM software helpful
*  Familiarity with Git and repositories useful
*  Proficiency in MS Excel/ Google Sheets
*  You are team-centric
*  You're a self starter, willing to read and watch in order to learn.  (Be ready to learn and how to use GitLab and Git)
*  Ability to use GitLab

## Marketing Operations Manager (Intermediate)

Your job as marketing operations manager is threefold: maintaining, optimizing, and integrating a set of marketing tech stacks, including Marketo and Outreach. You will work closely with business partners across the organization and help impact change and optimization. You will be able to drive key quarterly OKRs. 

### Job Grade 

The Marketing Operations Manager is a [grade 6](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Responsibilities

Marketing technology
*  Maintain Marketo’s performance and optimize processes. 
*  Create training documentation that guides the marketing team.
*  Train the marketing team on marketing software tools.
*  Audit use of marketing software tools with an eye towards continually improving how they are configured.

Marketing data stewardship
*  Be proactive and review functionality, workflow, and updating landing pages. 
*  Investigate data quality and continuously improve on data quality.  
*  Maintain Marketo database quality, ensure GDPR / CASL compliance.

Marketing data examination
*  Assist in measuring and investigating the different tools. 
*  Assist key business partners in measuring the effectiveness of marketing campaigns and marketing automation programs. 

### Requirements

All of the above requirements and:
*  Two plus years of experience in Marketing Operations or related role.
*  Bachelor's degree.  Is your college degree in French foreign politics with a minor in interpretive dance but you've been selling and marketing products since you were 12!  We understand that your degree isn't the only thing that prepares you as a potential job candidate.
*  Experience with modern marketing and sales development solutions such as Salesforce, LeanData, Bizible, ZoomInfo, RingLead, and Marketo.
*  Outreach experience is required.
*  You are obsessed with making customers happy. You know that the slightest trouble in getting started with a product can ruin customer happiness.
*  Be ready to learn how to use GitLab and Git.

## Senior Marketing Operations Manager

### Job Grade 

The Senior Marketing Operations Manager is a [grade 7](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Responsibilities

All of the above responsibilities and:
*  Oversee strategic and operational initiatives across marketing and with other functions to drive performance improvements, continuously enhance the impact of marketing and help the company continue along its fast growth trajectory.
*  Collaborate across functions to structure problems, conduct analysis, and drive to solutions through a rigorous, data-driven process.
*  Create and manage project plans with clearly defined deliverables and resources, coordinate work streams and dependencies, track and communicate progress, and identify obstacles and ensure they are addressed.

### Requirements

All of the above requirements and:
*  Excellent spoken and written English.
*  5+ years of experience, including more technical expertise.
*  Excellent analytical and problem-solving skills.
*  Exemplary oral and written communication skills, including ability to concisely present project deliverables.
*  Ability to work successfully with little guidance.

## Manager, Marketing Operations

### Job Grade 

The Manager, Marketing Operations is a [grade 8](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Responsibilities

*  Effectively influence, align, and collaborate with key functions, particularly Sales, Finance, Compliance, and Legal.
*  Recruit, develop and guide a team to execute on key marketing strategies.

### Requirements

*  8+ years of experience with go-to-market operations strategy and business analytics.
*  5+ years of experience managing marketing vendors and/or team management.
*  Ability to work collaboratively, internally and externally.
*  Ability to manager/prioritize multiple projects and adapt to a changing, fast-paced environment.
*  Demonstrated management skill and ability to oversee a team and work in a group environment, including collaboration with Senior/Executive level leadership.
*  Possess exemplary interpersonal skills including influencing, negotiations and teamwork skills.
*  Ability to think strategically, develop frameworks and platforms that ensure optimal and unfettered access to business tools, assets and capabilities.
*  Superb analytical, problem-solving capability and help the team make sound conclusions.
*  You believe in teamwork and are not afraid of rolling up your sleeves.

### Career Ladder 
The next step in a Manager, Marketing Operations' career at GitLab is to be a Senior Manager, Marketing Operations which is not yet defined. The next step after that would be [Director, Marketing Operations.](https://about.gitlab.com/job-families/marketing/director-marketing-operations/).

### Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our [team page](/company/team/).
* Qualified candidates will be invited to schedule a 15 minute [screening call](/handbook/hiring/interviewing/#conducting-a-screening-call) with one of our Global Recruiters after completing our questionnaire provided.
* A 30 minute interview with future co-worker/s (Marketing Operations Manager)
* A 45 minute interview with future manager (Director, Marketing Operations)
* A 30 minute interview with future marketing partner/s based on specialization
* Successful candidates will subsequently be made an offer via email

Additional details about our process can be found on our [hiring page](/handbook/hiring/).

### Relevant Links

[Marketing Handbook](/handbook/marketing/)
