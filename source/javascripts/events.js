// To add/remove filters add/remove the associated html in the haml file and add/remove from this list.
const filters = [{
  name: 'type',
  id: 'event-types',
  title: 'Any Event Type',
  options: [{ name: 'Any Event Type', id: 'type-any-event-type' }]
}, {
  name: 'location',
  id: 'event-location',
  title: 'Any Location Type',
  options: [{ name: 'Any Location Type', id: 'location-any-location-type' }]
}, {
  name: 'region',
  id: 'event-region',
  title: 'Any Region Type',
  options: [{ name: 'Any Region Type', id: 'region-any-region-type' }]
}];

const maxEventsPerPage = 25;
const $allEvents = document.querySelectorAll('#event-list li');
let $filteredEvents = [];
let currentPage = 1;
let numPages = 1;

// Queries all the events in the dom and returns data from each of their data attributes.
const getEventData = () => {  
  const eventData = [];
  $allEvents.forEach(($event) => {
    eventData.push(Object.assign(
      { element: $event },
      ...filters.map((filter) => ({ 
        [filter.name]: $event.getAttribute(`data-${filter.name}`)
      })
    )));
  });
  return eventData;
};

// Finds all the unique filter drop down options and adds it to the filters object
const populateFiltersWithOptions = (eventData) => {
  eventData.map((data) => {
    filters.map((filter) => {
      if (!filter.options.some((filterOption) => filterOption.name === data[filter.name])) {
        filter.options.push({
          name: data[filter.name],
          id: `${filter.name} ${data[filter.name]}`.replace(/\s+/g, '-').toLowerCase()
        });
      }
    })
  });
};

// Updates the selected item in the dropdown
const renderFilterTitle = (filter) => {
  const $filter = document.getElementById(filter.id);
  $filter.querySelector('.dropdown-title').innerHTML = filter.title;
};

const updateQueryParams = () => {
  const queryParams = new URLSearchParams(window.location.search);
  filters.map((filter, index) => {
    queryParams.set(filter.name, filter.title);
  });
  history.replaceState(null, null, `?${queryParams.toString()}`.toLowerCase());
}

// Creates/Updates the items in the filter dropdown
const renderFilterOptions = (filter) => {
  const $filterMenu = document.getElementById(filter.id).querySelector('.dropdown-menu');
  filter.options.map((option) => {
    let $option = document.getElementById(option.id);
    if ($option) {
      $option.style.display = option.name === filter.title ? 'none' : 'list-item';
    } else {
      $option = document.createElement('li');
      $option.id = option.id;
      $option.innerHTML = option.name;
      $option.style.display = option.name === filter.title ? 'none' : 'list-item';
      $filterMenu.appendChild($option);
    }
  });
};

// Updates the list of elements in the dom to match the selected filters
const applyFilters = () => {
  const $eventListContainer = document.getElementById('event-list');
  $eventListContainer.innerHTML = '';
  $filteredEvents = [];

  document.querySelectorAll('.event-page').forEach((eventPage) => eventPage.remove());

  $allEvents.forEach(($event) => {
    if (filters.every((filter) => $event.dataset[filter.name] === filter.title || filter.title.startsWith('Any'))) {
      $filteredEvents.push($event);
    }
  });

  const $eventsOnPage = $filteredEvents.slice((currentPage-1) * maxEventsPerPage, currentPage * maxEventsPerPage);
  $eventsOnPage.forEach(($event) => {
    $eventListContainer.appendChild($event);
  });

  const $pagination = document.getElementById('pagination');
  $pagination.style.display = $filteredEvents.length > maxEventsPerPage ? 'inline-flex' : 'none';

  if ($filteredEvents.length > maxEventsPerPage) {
    numPages = Math.ceil($filteredEvents.length / maxEventsPerPage);
    for (let index = 0; index < numPages; index++) {
      const $eventPage = document.createElement('li');
      $eventPage.classList.add('event-page'); 
      $eventPage.innerHTML = index + 1;

      $pagination.insertBefore($eventPage, document.getElementById('next-event-page'))
    }
  }
};

// Adds event listeners to the items in the filter dropdown
const addEventListeners = (filter) => {
  filter.options.map((option) => {
    document.getElementById(option.id).addEventListener('click', (event) => {
      const filter = filters.find((filter) => filter.id === event.target.closest('.dropdown').id);
      filter.title = event.target.innerHTML;
      renderFilterTitle(filter);
      updateQueryParams();
      renderFilterOptions(filter);
      currentPage = 1;
      numPages = 1;
      applyFilters();
    });
  });
};

document.addEventListener("DOMContentLoaded", () => {
  const eventData = getEventData();
  populateFiltersWithOptions(eventData);

  // Example: about.gitlab.com/events?type=webcast&location=virtual&region=amer
  const queryString = window.location.search;
  const urlParams = new URLSearchParams(queryString);
  let hasValidFilter = false;
  filters.map((filter) => {
    const urlFilter = urlParams.get(filter.name);
    if (urlFilter) {
      filter.options.map((filterOption) => {
        if (filterOption.name.toLowerCase() === urlFilter) {
          filter.title = filterOption.name;
          hasValidFilter = true;
        }
      });
    }
  });

  filters.map(filter => {
    renderFilterTitle(filter);
    renderFilterOptions(filter);
    addEventListeners(filter);
  });

  applyFilters();

  // Pagination
  document.getElementById('previous-event-page-button').addEventListener('click', (event) => {
    if (currentPage !== 1) {
      currentPage -= 1;
      applyFilters();
      document.getElementById('event-list').scrollIntoView();
    }
  });
  
  document.getElementById('next-event-page-button').addEventListener('click', (event) => {
    if (currentPage !== numPages) {
      currentPage += 1;
      applyFilters();
      document.getElementById('event-list').scrollIntoView();
    }
  });
});
